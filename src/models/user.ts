import mongoose, { Schema } from 'mongoose';

const UserScheme: Schema = new Schema({
  fullname: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  admin: {
    type: Boolean,
    default: false
  },
  dev: {
    type: Boolean,
    default: false
  },
  password: {
    type: String,
    required: true
  },
  authToken: {
    type: String,
    required: false
  }
});

export const User = mongoose.model('Users', UserScheme);

export interface IUser {
  _id?: string;
  password: string;
  fullname: string;
  username: string;
  authToken?: string;
  admin: boolean;
  dev: boolean;
}
