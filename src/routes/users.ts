import express, { Response, Router, Request } from 'express';
import { AuthenticationController } from '../controller/authentication-controller';

import { User } from '../models/user';
import {
  errorHandler,
  isAuthenticated,
  isAuthenticatedAndAdmin,
} from '../handler/handler';

export class UserRoute {
  private router: Router;
  private authController: AuthenticationController;
  constructor(controller: AuthenticationController) {
    this.authController = controller;
    this.router = express.Router();
    this.setup();
  }

  public get usersRouter() {
    return this.router;
  }

  private setup() {
    this.router.put(
      '/',
      isAuthenticated,
      async (request: Request, res: Response) => {
        if (request.body.oldUsername == '') {
          res.status(200).send(await this.authController.register(request));
        } else {
          res.status(200).send(await this.authController.update(request));
        }
      }
    );

    this.router.delete(
      '/:username',
      isAuthenticatedAndAdmin,
      async (request: Request, res: Response) => {
        await User.deleteOne({ username: request.params.username });
        res.status(200).send({});
      }
    );

    this.router.get(
      '/',
      isAuthenticated,
      async (request: Request, res: Response) => {
        res.status(200).send(await User.find({}));
      }
    );

    this.router.get(
      '/',
      isAuthenticated,
      async (request: Request, res: Response) => {
        res.status(200).send(await User.find({}));
      }
    );

    this.router.get(
      '/all',
      isAuthenticated,
      async (request: Request, res: Response) => {
        res.status(200).send(await User.find({}));
      }
    );

    this.router.get(
      '/one/:id',
      isAuthenticated,
      async (request: Request, res: Response) => {
        res.status(200).send(await User.findOne({ _id: request.params.id }));
      }
    );
  }
}
