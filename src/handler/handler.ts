import { NextFunction, Response, Request } from 'express';
import { AuthenticationController } from '../controller/authentication-controller';
const ERRTYPE_NOTFOUND = 'webserver/ERRTYPE_NOTFOUND';
import * as jwt from 'jsonwebtoken';
export function errorHandler(err: any, req: Request, response: Response, next: NextFunction) {
  console.error(err);
  if (err.type && err.type === AuthenticationController.ERRTYPE_UNAUTHORIZED) {
    response.status(401).send(err.msg);
  } else if (err.type && err.type === AuthenticationController.ERRTYPE_FORBIDDEN) {
    response.status(403).send(err.msg);
  } else if (err.type && err.type === ERRTYPE_NOTFOUND) {
    response.status(404).send(err.msg);
  } else {
    response.status(500).send(JSON.stringify(err));
  }
  next();
}

export function isAuthenticatedAndAdmin(req: Request, res: Response, next: NextFunction) {
  if (!req.headers || !req.headers['x-auth-token']) {
    throw {
      type: AuthenticationController.ERRTYPE_UNAUTHORIZED,
      msg: 'UNAUTHORIZED'
    };
  }
  jwt.verify(req.headers['x-auth-token'] as string, process.env.SECRET as string, (err: any, decoded: any) => {
    if (err) {
      if (err.name == 'TokenExpiredError') {
        throw {
          type: AuthenticationController.ERRTYPE_FORBIDDEN,
          msg: 'FORBIDDEN'
        };
      }
      throw {
        type: AuthenticationController.ERRTYPE_UNAUTHORIZED,
        msg: 'UNAUTHORIZED'
      };
    }
    if (!decoded.user.admin) {
      throw {
        type: AuthenticationController.ERRTYPE_UNAUTHORIZED,
        msg: 'UNAUTHORIZED'
      };
    }
    req.user = decoded.user;
    next();
  });
}

export function isAuthenticated(req: Request, res: Response, next: NextFunction) {
  if (!req.headers || !req.headers['x-auth-token']) {
    throw {
      type: AuthenticationController.ERRTYPE_UNAUTHORIZED,
      msg: 'UNAUTHORIZED'
    };
  }
  jwt.verify(req.headers['x-auth-token'] as string, process.env.SECRET as string, (err: any, decoded: any) => {
    if (err) {
      if (err.name == 'TokenExpiredError') {
        throw {
          type: AuthenticationController.ERRTYPE_FORBIDDEN,
          msg: 'FORBIDDEN'
        };
      }
      throw {
        type: AuthenticationController.ERRTYPE_UNAUTHORIZED,
        msg: 'UNAUTHORIZED'
      };
    }
    req.user = decoded.user;
    next();
  });
}
