import { IUser } from './../models/user';
import { Request } from 'express';
import md5 from 'md5';
import { User } from '../models/user';
import * as jwt from 'jsonwebtoken';
export class AuthenticationController {
  public static ERRTYPE_UNAUTHORIZED = 'AuthController/ERR_UNAUTHORIZED';
  public static ERRTYPE_FORBIDDEN = 'AuthController/ERR_FORBIDDEN';
  public static ERRTYPE_UNKNOWN = 'AuthController/ERR_UNKNOWN';
  private static AUTH_JSON = 'data/user.json';

  //  Login funktion
  public logIn(username: string, password: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        let user: any = await User.findOne({ username: username });
        user = user?.toObject();
        // Prüfe ob username existiert
        if (!user) {
          reject({
            type: AuthenticationController.ERRTYPE_UNAUTHORIZED,
            msg: 'User nicht gefunden',
          });
          return;
        }
        // Prüfe ob das verschlüsselte Passwort mit dem Username übereinstimmt
        if (md5(password) !== user.password) {
          reject({
            type: AuthenticationController.ERRTYPE_UNAUTHORIZED,
            msg: 'Passwort stimmt nicht überein',
          });
          return;
        }
        delete user.authToken;
        const authToken = jwt.sign(
          {
            user,
          },
          process.env.SECRET as string,
          {
            expiresIn: 60 * 60 * 72,
          }
        );
        user.authToken = authToken;
        await User.updateMany(
          { _id: user._id },
          { $set: { authToken: authToken } }
        );
        resolve([user, authToken]);
      } catch (err) {
        if (!err.type && !err.msg) {
          err = {
            type: AuthenticationController.ERRTYPE_UNKNOWN,
            msg: err.toString(),
            cause: err,
          };
        }
        reject(err);
      }
    });
  }

  public refresh(request: Request): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        if (!request.body.token) {
          reject({
            type: AuthenticationController.ERRTYPE_UNAUTHORIZED,
            msg: 'User nicht gefunden',
          });
        }
        let user: any = await User.findOne({
          authToken: request.body.token,
        });
        user = user?.toObject();
        if (!user) {
          reject({
            type: AuthenticationController.ERRTYPE_UNAUTHORIZED,
            msg: 'User nicht gefunden',
          });
          return;
        }
        delete user.authToken;
        const authToken = await jwt.sign(
          {
            user,
          },
          process.env.SECRET as string,
          {
            expiresIn: 60 * 60 * 72,
          }
        );
        await User.updateMany(
          { _id: user._id },
          { $set: { authToken: authToken } }
        );
        resolve(authToken);
      } catch (err) {
        if (!err.type && !err.msg) {
          err = {
            type: AuthenticationController.ERRTYPE_UNKNOWN,
            msg: err.toString(),
            cause: err,
          };
        }
        reject(err);
      }
    });
  }

  public register(request: Request): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const reqUser: IUser | undefined = request.user;
        if (!reqUser?.admin) {
          throw {
            type: AuthenticationController.ERRTYPE_UNAUTHORIZED,
            msg: 'UNAUTHORIZED',
          };
        }
        const body = request.body;
        body.password = md5(body.newPw1);
        delete body._id;
        resolve(await new User(body).save());
      } catch (err) {
        if (!err.type && !err.msg) {
          err = {
            type: AuthenticationController.ERRTYPE_UNKNOWN,
            msg: err.toString(),
            cause: err,
          };
        }
        reject(err);
      }
    });
  }

  public update(request: Request): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const reqUser: IUser | undefined = request.user;
        const body = request.body;
        if (body.oldUsername == reqUser?.username || reqUser?.admin) {
          let user: any = await User.findOne({ _id: body._id });
          let obj: any = {
            fullname: body.fullname,
            username: body.username,
          };
          if (
            body.oldPw &&
            user.password == md5(body.oldPw) &&
            md5(body.newPw1) == md5(body.newPw2)
          ) {
            obj.password = md5(body.newPw1);
          }
          user = Object.assign(user, obj);

          resolve(await User.updateOne({ _id: user._id }, user));
        }
      } catch (err) {
        if (!err.type && !err.msg) {
          err = {
            type: AuthenticationController.ERRTYPE_UNKNOWN,
            msg: err.toString(),
            cause: err,
          };
        }
        reject(err);
      }
    });
  }

  public authorize(authToken: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const user = await User.findOne({ authToken: authToken });
        if (user === null) {
          resolve({});
        } else {
          resolve(user);
        }
      } catch (err) {
        if (!err.type && !err.msg) {
          err = {
            type: AuthenticationController.ERRTYPE_UNKNOWN,
            msg: err.toString(),
            cause: err,
          };
        }
        reject(err);
      }
    });
  }

  private generateAuthToken(username: string): string {
    const base = username + new Date().getTime() + Math.random().toFixed(10);
    return md5(base);
  }

  //
  private getUserByAuthToken(
    users: any,
    authToken: string,
    dontKillPassword?: boolean
  ) {
    // gehe durch jedes Objekt
    for (let u of users) {
      // Wenn der authToken im Objekt mit dem übergebenen authToken übereinstimmt
      if (u.authToken === authToken) {
        // wenn dont KillPassword true ist dann lösche das (unverschlüsselte) Passwort
        if (!dontKillPassword) {
          delete u.password;
        }
        return u;
      }
    }
    // ansonsten gebe null zurück
    return null;
  }
}
