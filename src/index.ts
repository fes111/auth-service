import { UserRoute } from './routes/users';
import express, { Request, Response, Application } from 'express';
import * as bodyParser from 'body-parser';
import cors from 'cors';
import { errorHandler } from './handler/handler';
import { AuthenticationController } from './controller/authentication-controller';
import mongoose from 'mongoose';
require('dotenv').config();

const PORT = process.env.PORT || 8445;
const authCtrl = new AuthenticationController();
const app: Application = express();

app.use(cors());
app.use(bodyParser.json());

app.get('/', (req: Request, res: Response) => {
  res.send('express-template');
});

app.post('/refresh', async (request: Request, response: Response) => {
  try {
    const token = await authCtrl.refresh(request);
    response.status(200).send({ token, status: true });
  } catch (error) {
    response.status(200).send({ status: false });
  }
});

app.post('/login', async (request: Request, response: Response) => {
  const password = request.body.password;
  const username = request.body.username;
  let [user, token] = await authCtrl.logIn(username, password);
  user.authToken = token;
  response.status(200).send(user);
});

app.use('/users', new UserRoute(authCtrl).usersRouter);

app.use(errorHandler);

app.listen(PORT, async () => {
  await mongoose.connect(process.env.MONGO_URL as string, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  console.log(`Application running at: http://localhost:${PORT}/`);
});
