import { IBoard } from './../src/models/board.model';
import { IUser } from '../src/models/user.model';
declare global {
  namespace Express {
    export interface Request {
      user: IUser;
      board: IBoard;
    }
  }
}
