FROM node:11
USER node

ARG BUILD_VARIANT
ARG APP=app
ARG HOME=/home/node

ENV NPM_CONFIG_PREFIX=$HOME/.npm-global
ENV PATH=$PATH:$HOME/.npm-global/bin

RUN mkdir -p /home/node/app
WORKDIR /home/node/app

COPY --chown=node:node ./auth/package.json ./
RUN npm install

COPY --chown=node:node ./auth/ ./
RUN npm run tsc

CMD [ "npm", "run", "run:docker" ]